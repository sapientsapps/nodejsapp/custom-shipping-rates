import mysql from 'mysql'

const Conn = () => {
    const con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "12345",
        database: "node_react_app"
    });

    con.connect(function (err) {
        if (err) throw err;
        console.log("Connected!");
    });

    //console.log(con)

    return con
}

export default Conn;

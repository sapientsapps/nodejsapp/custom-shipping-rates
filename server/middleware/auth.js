import { Shopify, DataType } from "@shopify/shopify-api";

import topLevelAuthRedirect from "../helpers/top-level-auth-redirect.js";

import Conn from "../database.js";
const db = Conn();

export default function applyAuthMiddleware(app) {
  app.get("/auth", async (req, res) => {
    if (!req.signedCookies[app.get("top-level-oauth-cookie")]) {
      return res.redirect(
        `/auth/toplevel?${new URLSearchParams(req.query).toString()}`
      );
    }

    const redirectUrl = await Shopify.Auth.beginAuth(
      req,
      res,
      req.query.shop,
      "/auth/callback",
      app.get("use-online-tokens")
    );

    res.redirect(redirectUrl);
  });

  app.get("/auth/toplevel", (req, res) => {
    res.cookie(app.get("top-level-oauth-cookie"), "1", {
      signed: true,
      httpOnly: true,
      sameSite: "strict",
    });

    res.set("Content-Type", "text/html");

    res.send(
      topLevelAuthRedirect({
        apiKey: Shopify.Context.API_KEY,
        hostName: Shopify.Context.HOST_NAME,
        host: req.query.host,
        query: req.query,
      })
    );
  });

  app.get("/auth/callback", async (req, res) => {
    try {
      const session = await Shopify.Auth.validateAuthCallback(
        req,
        res,
        req.query
      );

      //INSERTING TOKEN AND SHOP IN DATABASE

      let resultLength;
      let token = session.accessToken;
      let shopName = session.shop;
      console.log("TOKEN", token);
      console.log("SHOP", shopName);
      console.log("Hello World!!");
      var sqlSelect = `SELECT * FROM shop_auth_webhook_test`;
      const data = await getData(sqlSelect, db);
      console.log(data.length);
      resultLength = data.length;
      console.log("Bye World!!");

      console.log("Result Length:", resultLength);

      if (resultLength == 0) {
        var sql = `INSERT INTO shop_auth_webhook_test (access_token, shop_name) VALUES ('${token}', '${shopName}')`;
        db.query(sql, function (err, result) {
          if (err) throw err;
          console.log("Data inserted!");
          console.log(result);
          //res.send("Data inserted!!");
        });
      } else {
        var sql = `UPDATE shop_auth_webhook_test SET access_token = '${token}', shop_name = '${shopName}'`;
        db.query(sql, function (err, result) {
          if (err) throw err;
          console.log("Data updated!");
          console.log(result);
          //res.send("Data updated!!");
        });
      }

      //********************************************************** */

      /**************************SHIPPING RATE API *****************/

      getShippingRates();

      /************************************************************ */

      const host = req.query.host;
      app.set(
        "active-shopify-shops",
        Object.assign(app.get("active-shopify-shops"), {
          [session.shop]: session.scope,
        })
      );

      const response = await Shopify.Webhooks.Registry.register({
        shop: session.shop,
        accessToken: session.accessToken,
        topic: "APP_UNINSTALLED",
        path: "/webhooks",
      });

      if (!response["APP_UNINSTALLED"].success) {
        console.log(
          `Failed to register APP_UNINSTALLED webhook: ${response.result}`
        );
      }

      // Redirect to app with shop parameter upon auth
      res.redirect(`/?shop=${session.shop}&host=${host}`);
    } catch (e) {
      switch (true) {
        case e instanceof Shopify.Errors.InvalidOAuthError:
          res.status(400);
          res.send(e.message);
          break;
        case e instanceof Shopify.Errors.CookieNotFound:
        case e instanceof Shopify.Errors.SessionNotFound:
          // This is likely because the OAuth session cookie expired before the merchant approved the request
          res.redirect(`/auth?shop=${req.query.shop}`);
          break;
        default:
          res.status(500);
          res.send(e.message);
          break;
      }
    }
  });

  //DB FUNCTIONS
  const performAsynScanOperation = async (scanParams, dynamoDB) => {
    return new Promise((resolve, reject) => {
      db.query(scanParams, function (err, responseData) {
        if (err) {
          reject(`${err}`);
        } else {
          resolve(responseData);
        }
      });
    });
  };

  const getData = async (scanParams, dynamoDB) => {
    let value;
    const data = await performAsynScanOperation(scanParams, dynamoDB);
    value = data;
    return value;
  };

  //POST SHIPPING RATES
  const getShippingRates = async () => {
    var token, shopName;
    var sql = `SELECT * FROM shop_auth_webhook_test`;
    const dbData = await getData(sql, db);
    token = dbData[0].access_token;
    shopName = dbData[0].shop_name;

    console.log(token);
    console.log(shopName);

    const client = new Shopify.Clients.Rest(shopName, token);
    const data = await client.get({
      path: "carrier_services",
    });

    console.log(data.body.carrier_services.length, "length");
    console.log(data.body.carrier_services);

    let carrierServiceArray = [data.body.carrier_services[0]];

    var flag = 0;

    if (data.body.carrier_services.length > 0) {
      //console.log(carrierServiceArray);
      carrierServiceArray.forEach((item, index) => {
        console.log("in condition");
        console.log(item);
        if (item.name == "Custom Shipping Rate") {
          //console.log(item.name);
          console.log(flag, "condition flag");
          return (flag = 1);
        }
      });
    }
    console.log(flag, "flag");
    if (flag == 1) {
      console.log("Already Exist!");
    } else {
      //let token, shopName;
      console.log("Created new rate");
      var sql = `SELECT * FROM shop_auth_webhook_test`;
      const dbData = await getData(sql, db);
      token = dbData[0].access_token;
      shopName = dbData[0].shop_name;

      console.log(token);
      console.log(shopName);

      const client = new Shopify.Clients.Rest(shopName, token);
      const data = await client.post({
        path: "carrier_services",
        data: {
          carrier_service: {
            name: "Custom Shipping Rate",
            callback_url: `${process.env.HOST}/callbackShippingRates`,
            service_discovery: true,
          },
        },
        type: DataType.JSON,
      });

      console.log(data);
      res.send(data);
    }
    //res.send(data);
  };
}
